process.env.NODE_ENV = 'test';
let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();
var server = require('../app');

chai.use(chaiHttp);

describe('/GET token-info', () => {
      it('This should GET the token info', (done) => {
        chai.request(server)
            .get('/token-info')
            .end((err, res) => {
                  res.should.have.status(200);
                  res.body.should.be.a('object');
                  res.body.data.should.be.a('object');
                  res.body.data.w1.should.not.equal(0);
                  res.body.data.w2.should.not.equal(0);
                  res.body.data.w3.should.not.equal(0);
                  res.body.data.w1daa.should.not.equal(0);
                  res.body.data.w2daa.should.not.equal(0);
                  res.body.data.w3daa.should.not.equal(0);
              done();
            });
      });
  });
