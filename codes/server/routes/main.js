// Main route
module.exports = (app) => {
  app.get('/', (req, res) => {
    res.send("A Node.js server is running on port 5000!");
  });
}
