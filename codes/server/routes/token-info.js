// Token info route

var workweek2sec = 432000;
var block_gen_time = 14;
var tokenAddress = '';

var abi = require('human-standard-token-abi');
const Web3 = require('web3');
const web3 = new Web3('https://mainnet.infura.io/v3/4e062eca5c3a498c933057b9ae6e7077');

function calcEvents(week, events) {
  var counter = 0
  for(var i = 0; i<events.length; i++) {
    var event = events[i];
    if(event.blockNumber <= week.end && event.blockNumber > week.start) {
      counter += 1
    }
  }
  return counter;
}

function calcDAA(week, events) {
  var usersSet = new Set();
  for(var i = 0; i<events.length; i++) {
    var event = events[i];
    if(event.blockNumber <= week.end && event.blockNumber > week.start) {
      usersSet.add(event.returnValues[0]);
      usersSet.add(event.returnValues[1]);
    }
  }
  return usersSet.size;
}

module.exports = (app) => {

  app.get('/token-info', async (req, res) => {
    try {
      var data = {
        name: "",
        symbol: "",
        decimals: "",
        supply: "",
        address: "",
        addressBalance: "",
        w1: 0,
        w2: 0,
        w3: 0,
        w1daa: 0,
        w2daa: 0,
        w3daa: 0
      }
      var contractAddress = "0xd26114cd6EE289AccF82350c8d8487fedB8A0C07" //OmiseGo
      var tokenContract = new web3.eth.Contract(abi, contractAddress);
      data.name = await tokenContract.methods.name().call();
      data.symbol = await tokenContract.methods.symbol().call();
      data.decimals = await tokenContract.methods.decimals().call();
      data.supply = await tokenContract.methods.totalSupply().call();
      data.address = tokenAddress;
      if(tokenAddress != '') {
        var balance = await tokenContract.methods.balanceOf(tokenAddress).call();
        data.addressBalance = web3.utils.fromWei(web3.utils.toBN(balance)) + " OMG";
      }

      var firstBlock = await web3.eth.getBlockNumber();
      var weeks = {
        week1: {
          start: firstBlock - ((3*workweek2sec)/block_gen_time|0),
          end: firstBlock - ((2*workweek2sec)/block_gen_time|0)
        },
        week2: {
          start: firstBlock - ((2*workweek2sec)/block_gen_time|0),
          end: firstBlock - (workweek2sec/block_gen_time|0)
        },
        week3: {
          start: firstBlock - (workweek2sec/block_gen_time|0),
          end: firstBlock
        }
      }
      var events = await tokenContract.getPastEvents('Approval', {
        fromBlock: weeks.week1.start,
        toBlock: 'latest'
      });

      if(events) {

        data.w1 = calcEvents(weeks.week1, events);
        data.w2 = calcEvents(weeks.week2, events);
        data.w3 = calcEvents(weeks.week3, events);

        data.w1daa = calcDAA(weeks.week1, events);
        data.w2daa = calcDAA(weeks.week2, events);
        data.w3daa = calcDAA(weeks.week3, events);

      }

      res.send({data});
    } catch (e) {
      console.error("Token-Info error: "+e.message);
      res.redirect('/error');
    }
  });

  app.post('/fetch-token-info', (req, res) => {
    tokenAddress = req.body.address;
    if(!tokenAddress) {
      res.redirect('/error')
    } else {
      res.redirect('/show-token-info');
    }
  });
}
