import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import LoadingIcon from '../assets/loading-icon.png';
import ChartistGraph from 'react-chartist';
import Legend from "chartist-plugin-legend";

class TokenInfo extends Component {
	constructor(props) {
		super(props);
		   this.state = ({
		      isLoading: true,
		      tokenName: '',
					tokenSymbol: '',
		      tokenDecimals: '',
		      tokenSupply: '',
					tokenAddress: '',
					tokenAddressBalance: '',
					chartData: {
            labels: ['Week1', 'Week2', 'Week3'],
    				series: [
							{ name: 'DAA', 'data': [0, 0, 0] },
							{ name: 'Tx', 'data': [0 ,0 ,0] }
						],
						backgroundColor: ['red', 'orange']
					},
        chartOptions: {
          high: 350,
          low: 0,
					plugins: [
						Legend()
					]
      	}
		})
	}

	componentDidMount() {
		fetch('/token-info')
		.then(res => res.json())
		.then(data => {
			if(data.data.cod === '404') {
				this.setState({
					isLoading: false,
					tokenInfoNotFound: '404'
				})
			} else {
				this.setState({
					isLoading: false,
					tokenName: data.data.name,
					tokenSymbol: data.data.symbol,
					tokenDecimals: data.data.decimals,
					tokenSupply: data.data.supply,
					tokenAddress: data.data.address,
					tokenAddressBalance: data.data.addressBalance
				});
				this.state.chartData.series[0].data[0] = data.data.w1daa;
				this.state.chartData.series[0].data[1] = data.data.w2daa;
				this.state.chartData.series[0].data[2] = data.data.w3daa;
				this.state.chartData.series[1].data[0] = data.data.w1;
				this.state.chartData.series[1].data[1] = data.data.w2;
				this.state.chartData.series[1].data[2] = data.data.w3;
				this.forceUpdate();
				console.log("Data : ",data);
				console.log("ChartData: ", this.state.chartData);
			}
		})
		.catch(err => {
		   console.log(err);
		})
	}

	render() {
		const TokenInfoError = (
			<div className='tokenInfo'>
				<div className='tokenInfoError'>
							<p> Something went wrong with this token... </p>
					 <Link to='/'><button>Go back to homepage </button></Link>
				</div>
			</div>
		)

		const TokenInfoScreen = (
			this.state.tokenInfoNotFound == 404 ? <div> { TokenInfoError } </div> :
			<div>
				<div className='homeButton'>
				     <Link to='/'><button>Back to homepage</button></Link>
			   </div>
			   <div className='tokenInfo'>
			     <h4> Name : {this.state.tokenName} </h4>
					 <h4> Symbol : {this.state.tokenSymbol} </h4>
					 <h4> Decimals : {this.state.tokenDecimals} </h4>
					 <h4> Supply : {this.state.tokenSupply} </h4>
					 <h4> Address : {this.state.tokenAddress} | Balance : {this.state.tokenAddressBalance} </h4>
			   </div>
				 <div>
           <ChartistGraph className='ct-octave' data={this.state.chartData} options={this.state.chartOptions} type='Bar' />
				 </div>
			</div>
		)

		const LoadingScreen = (
		   <div className='loading'>
				 <img className='loadingIcon' src={LoadingIcon} alt='loading-icon'/>
		   </div>
		)

		const MainTokenInfoScreen = (
			this.state.isLoading === true ? <div> {LoadingScreen} </div> : <div> {TokenInfoScreen} </div>
		)

		return (
		   <div> { MainTokenInfoScreen } </div>
		)
	}
}

export default TokenInfo;
