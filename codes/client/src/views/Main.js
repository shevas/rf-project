import React, { Component } from 'react';
const Main = () => {

   return (
      <div>
         <div className='mainTitle'>
            <h2>Token Metrics (OmiseGo Token)</h2>
         </div>

         <div>
            <p>Enter ERC20 OMG token's address or use a working sample (used for checking the balance on inserted address)</p>
         </div>

         <div className='addressInput'>
	          <form method='POST' action='/fetch-token-info'>
	           <input type='text' placeholder='Enter address' name='address'/>
	           <button>USE MY ADDRESS</button>
	          </form>
        </div>
        <div className='addressInput'>
          <form method='POST' action='/fetch-token-info'>
            <input type='text' value='0xa25d64854dc69438e0bcd0ff70f9163fc78d5b86' name='address' readOnly/>
            <button>USE SAMPLE ADDRESS</button>
          </form>
      </div>
      </div>
   )
}

export default Main
