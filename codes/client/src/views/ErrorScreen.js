import React, { Component } from 'react';
import { Link } from 'react-router-dom';

const ErrorScreen = () => {

   return (
     <div className='tokenInfo'>
       <div className='tokenInfoError'>
             <p> Something went wrong with this token... </p>
          <Link to='/'><button>Go back to homepage </button></Link>
       </div>
     </div>
   )
}

export default ErrorScreen
