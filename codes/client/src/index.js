// React components
import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Route, Switch } from 'react-router-dom';

// CSS styles
import './views/css/styles.scss';

// Views
import App from './views/App';
import Main from './views/Main';
import TokenInfo from './views/TokenInfo';
import ErrorScreen from './views/ErrorScreen';

ReactDOM.render(
	<Router>
	  <App>
		  <Route exact path='/' component={Main}/>
		  <Route exact path='/show-token-info' component={TokenInfo}/>
			<Route exact path='/error' component={ErrorScreen}/>
	  </App>
	</Router>

,document.getElementById('root'));
