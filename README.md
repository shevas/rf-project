# Assignment Project

Preparing the project and docker commands
---------------------------
In folders client and server install npm dependencies:

```angular2html
npm install
```

Then the project is ready to be dockerized. Navigate to 'codes' folder and run:
```angular2html
docker-compose up
```

To change between running inside a docker or locally, change the line inside package.json for 'client' folder:
*"proxy": "http://server:8080/" -> "proxy": "http://localhost:8080"*

 For testing, run inside server folder:
 ```angular2html
 npm test
 ```
